import numpy as np
import cv2
import pandas as pd
import matplotlib.pyplot as plt
import os
import pickle


img_dataset = pd.DataFrame()
img_path = 'Medtec-dataset/train_images/'

for image in os.listdir(img_path):
    
    df = pd.DataFrame()
    
    input_img = cv2.imread(img_path + image)
    
    if input_img.ndim == 3 and input_img.shape[-1] == 3:
        img = cv2.cvtColor(input_img, cv2.COLOR_BGR2GRAY)
    elif input_img.ndim == 2:
        img = input_img
    else:
        raise Exception("The module works only with grayscale and RGB images!")
        
    
    #Add original pixel values as feature
    pixel_values = img.reshape(-1)
    df['Original image'] = pixel_values
    
    #Add BGR channel values as features
    # blue_channel = input_img[:,:,0]
    # blue_values = blue_channel.reshape(-1)
    # df['Blue channel'] = blue_values
    
    # green_channel = input_img[:,:,1]
    # green_values = green_channel.reshape(-1)
    # df['Green channel'] = green_values
    
    # red_channel = input_img[:,:,2]
    # red_values = red_channel.reshape(-1)
    # df['Red channel'] = red_values
    
    
    #Add Gabor feature 
    num = 1
    kernels = []
    for theta in range(2):
        theta = theta / 4. * np.pi
        for sigma in (1, 3):
            for lambd in np.arange(0, np.pi, np.pi/4):
                for gamma in (0.05, 0.5):
                    
                    gabor_label = 'Gabor' + str(num)
                    ksize = 9
                    kernel = cv2.getGaborKernel((ksize,ksize), sigma, theta, lambd, gamma, 0, ktype=cv2.CV_32F)
                    kernels.append(kernel)
                    
                    fimg = cv2.filter2D(img, cv2.CV_8UC3, kernel)
                    filtered_img = fimg.reshape(-1)
                    df[gabor_label] = filtered_img
                    num += 1
                    
    #Add Canny edge
    edges = cv2.Canny(img, 100, 200)
    edges1 = edges.reshape(-1)
    df['Canny'] = edges1
    
    #Add other feature filters
    from skimage.filters import roberts, sobel, scharr, prewitt
    
    edge_roberts = roberts(img)
    edge_roberts1 = edge_roberts.reshape(-1)
    df['Roberts'] = edge_roberts1
    
    edge_sobel = sobel(img)
    edge_sobel1 = edge_sobel.reshape(-1)
    df['Sobel'] = edge_sobel1
    
    edge_scharr = scharr(img)
    edge_scharr1 = edge_scharr.reshape(-1)
    df['Scharr'] = edge_scharr1
    
    edge_prewitt = prewitt(img)
    edge_prewitt1 = edge_prewitt.reshape(-1)
    df['Prewitt'] = edge_prewitt1
    
    #GAUSSIAN with sigma=3
    from scipy import ndimage as nd
    gaussian_img = nd.gaussian_filter(img, sigma=3)
    gaussian_img1 = gaussian_img.reshape(-1)
    df['Gaussian s3'] = gaussian_img1
    
    #GAUSSIAN with sigma=7
    gaussian_img2 = nd.gaussian_filter(img, sigma=7)
    gaussian_img3 = gaussian_img2.reshape(-1)
    df['Gaussian s7'] = gaussian_img3
    
    #MEDIAN with sigma=3
    median_img = nd.median_filter(img, size=3)
    median_img1 = median_img.reshape(-1)
    df['Median s3'] = median_img1
    
    #VARIANCE with size=3
    variance_img = nd.generic_filter(img, np.var, size=3)
    variance_img1 = variance_img.reshape(-1)
    df['Variance s3'] = variance_img1
    
    img_dataset = img_dataset.append(df)
    
    
mask_dataset = pd.DataFrame()
mask_path = "Medtec-dataset/train_masks/"

for mask in os.listdir(mask_path):
    if mask=="desktop.ini":
        continue
    df2 = pd.DataFrame()
    input_mask = cv2.imread(mask_path + mask)
    
    if input_mask.ndim == 3 and input_mask.shape[-1] == 3:
        label = cv2.cvtColor(input_mask, cv2.COLOR_BGR2GRAY)
    elif input_mask.ndim == 2:
        label = input_mask
    else:
        raise Exception("The module works only with grayscale and RGB images!")

    label_values = label.reshape(-1)
    df2['Label_Value'] = label_values
    
    mask_dataset = mask_dataset.append(df2)

print('\nApplying concat..')
dataset = pd.concat([img_dataset, mask_dataset], axis=1)

dataset.loc[dataset['Label_Value'] <= 10, 'Label_Value'] = 0
dataset.loc[dataset['Label_Value'] > 10, 'Label_Value'] = 255
#dataset = dataset[dataset.Label_Value > 127]

X = dataset.drop(labels = ["Label_Value"], axis=1)
y = dataset["Label_Value"].values


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state = 20)

from sklearn.ensemble import RandomForestClassifier
model = RandomForestClassifier(n_estimators=60, random_state=42)

print('\nFitting now..')
model.fit(X_train, y_train)
print('\nFitting done')
prediction_test = model.predict(X_test)

from sklearn import metrics
print("Acc=", metrics.accuracy_score(y_test, prediction_test))

feature_list = list(X.columns)
feature_imp = pd.Series(model.feature_importances_,index=feature_list).sort_values(ascending=False)
print(feature_imp)

model_name = "wound_model_new"
pickle.dump(model, open(model_name, 'wb'))

print('Finished')



# cv2.imshow('a',img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()