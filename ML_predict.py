import numpy as np
import cv2
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import os
from skimage.morphology import remove_small_objects


def feature_extraction(img):
    
    df = pd.DataFrame()
    
    img2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    #Add original pixel values as feature
    pixel_values = img2.reshape(-1)
    df['Original image'] = pixel_values
    
    # #Add BGR channel values as features
    # blue_channel = img[:,:,0]
    # blue_values = blue_channel.reshape(-1)
    # df['Blue channel'] = blue_values
    
    # green_channel = img[:,:,1]
    # green_values = green_channel.reshape(-1)
    # df['Green channel'] = green_values
    
    # red_channel = img[:,:,2]
    # red_values = red_channel.reshape(-1)
    # df['Red channel'] = red_values
    
    #Add Gabor feature 
    num = 1    
    kernels = []
    for theta in range(2):
        theta = theta / 4. * np.pi
        for sigma in (1, 3):
            for lambd in np.arange(0, np.pi, np.pi/4):
                for gamma in (0.05, 0.5):
                    
                    gabor_label = 'Gabor' + str(num)
                    ksize = 9
                    kernel = cv2.getGaborKernel((ksize,ksize), sigma, theta, lambd, gamma, 0, ktype=cv2.CV_32F)
                    kernels.append(kernel)
                    
                    fimg = cv2.filter2D(img2, cv2.CV_8UC3, kernel)
                    filtered_img = fimg.reshape(-1)
                    df[gabor_label] = filtered_img
                    num += 1
                    
    #Add Canny edge
    edges = cv2.Canny(img2, 100, 200)
    edges1 = edges.reshape(-1)
    df['Canny'] = edges1
    
    #Add other feature filters
    from skimage.filters import roberts, sobel, scharr, prewitt
    
    edge_roberts = roberts(img2)
    edge_roberts1 = edge_roberts.reshape(-1)
    df['Roberts'] = edge_roberts1
    
    edge_sobel = sobel(img2)
    edge_sobel1 = edge_sobel.reshape(-1)
    df['Sobel'] = edge_sobel1
    
    edge_scharr = scharr(img2)
    edge_scharr1 = edge_scharr.reshape(-1)
    df['Scharr'] = edge_scharr1
    
    edge_prewitt = prewitt(img2)
    edge_prewitt1 = edge_prewitt.reshape(-1)
    df['Prewitt'] = edge_prewitt1
    
    #GAUSSIAN with sigma=3
    from scipy import ndimage as nd
    gaussian_img = nd.gaussian_filter(img2, sigma=3)
    gaussian_img1 = gaussian_img.reshape(-1)
    df['Gaussian s3'] = gaussian_img1
    
    #GAUSSIAN with sigma=7
    gaussian_img2 = nd.gaussian_filter(img2, sigma=7)
    gaussian_img3 = gaussian_img2.reshape(-1)
    df['Gaussian s7'] = gaussian_img3
    
    #MEDIAN with sigma=3
    median_img = nd.median_filter(img2, size=3)
    median_img1 = median_img.reshape(-1)
    df['Median s3'] = median_img1
    
    #VARIANCE with size=3
    variance_img = nd.generic_filter(img2, np.var, size=3)
    variance_img1 = variance_img.reshape(-1)
    df['Variance s3'] = variance_img1
    
    return df

def skin_segmentation(img):
    
    h, w, c = img.shape
    img_ycrcb = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)
    
    lower_YCbCr_values = np.array((40, 133, 77), dtype = "uint8")
    upper_YCbCr_values = np.array((245, 173, 125), dtype = "uint8")
    
    mask_YCbCr = cv2.inRange(img_ycrcb, lower_YCbCr_values, upper_YCbCr_values)
    
    holes1 = mask_YCbCr.copy()
    holes2 = holes1
    holes3 = holes1
    holes4 = holes1
    
    cv2.floodFill(holes1, None, (0, 0), 255)
    holes1 = cv2.bitwise_not(holes1)
    
    cv2.floodFill(holes2, None, (0,h-1), 255)
    holes2 = cv2.bitwise_not(holes2)
    
    cv2.floodFill(holes3, None, (w-1,0), 255)
    holes3 = cv2.bitwise_not(holes3)
    
    cv2.floodFill(holes4, None, (w-1,h-1), 255)
    holes4 = cv2.bitwise_not(holes4)
    
    holes = cv2.bitwise_and(holes1, holes2, holes3, holes4)
    
    skin_mask = cv2.bitwise_or(mask_YCbCr, holes)

    return skin_mask
    


model_name = "wound_model_new"
model = pickle.load(open(model_name, 'rb'))


img_path = 'test_images/img/'
mask_path = 'test_images/msk/'
for image in os.listdir(img_path):
    print(image)
    img = cv2.imread(img_path + image)
    img2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    X = feature_extraction(img)
    prediction = model.predict(X)
    

############################
    
    
    segmented = prediction.reshape((img2.shape))
    
    #skin_mask = skin_segmentation(img)
    
    h, w, c = img.shape
    img_ycrcb = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)
    
    lower_YCbCr_values = np.array((40, 133, 77), dtype = "uint8")
    upper_YCbCr_values = np.array((245, 173, 125), dtype = "uint8")
    
    skin_mask = cv2.inRange(img_ycrcb, lower_YCbCr_values, upper_YCbCr_values)
    
    holes1 = skin_mask.copy()
    holes2 = holes1
    holes3 = holes1
    holes4 = holes1
    
    cv2.floodFill(holes1, None, (0, 0), 255)
    holes1 = cv2.bitwise_not(holes1)
    
    cv2.floodFill(holes2, None, (0,h-1), 255)
    holes2 = cv2.bitwise_not(holes2)
    
    cv2.floodFill(holes3, None, (w-1,0), 255)
    holes3 = cv2.bitwise_not(holes3)
    
    cv2.floodFill(holes4, None, (w-1,h-1), 255)
    holes4 = cv2.bitwise_not(holes4)
    
    holes = cv2.bitwise_and(holes1, holes2, holes3, holes4)
    
    skin_mask_filled = cv2.bitwise_or(skin_mask, holes)


    result_only_skin = cv2.bitwise_and(skin_mask_filled, segmented)
    result_without_skin = result_only_skin - skin_mask

    # holes11 = result_without_skin.copy()
    # holes21 = holes11
    # holes31 = holes11
    # holes41 = holes11
    # cv2.floodFill(holes11, None, (0, 0), 255)
    # holes11 = cv2.bitwise_not(holes11)
    
    # cv2.floodFill(holes21, None, (0,h-1), 255)
    # holes21 = cv2.bitwise_not(holes21)
    
    # cv2.floodFill(holes31, None, (w-1,0), 255)
    # holes31 = cv2.bitwise_not(holes31)
    
    # cv2.floodFill(holes41, None, (w-1,h-1), 255)
    # holes41 = cv2.bitwise_not(holes41)
    
    # holes0 = cv2.bitwise_and(holes11, holes21, holes31, holes41)
    
    #result_without_skin_filled = cv2.bitwise_or(holes11, result_without_skin)
    
    #Remove small regions
    # se1 = cv2.getStructuringElement(cv2.MORPH_RECT, (2,2))
    # se2 = cv2.getStructuringElement(cv2.MORPH_RECT, (2,2))
    # #mask = cv2.morphologyEx(result_without_skin, cv2.MORPH_CLOSE, se1)
    # mask = cv2.morphologyEx(result_without_skin, cv2.MORPH_OPEN, se2)
    # out = result_without_skin * mask

    #Accuracy score
    from sklearn import metrics
    img_mask = cv2.imread(mask_path + image)
    ground_truth = cv2.cvtColor(img_mask, cv2.COLOR_BGR2GRAY)
    prediction_final = result_without_skin.copy()
    h = ground_truth.shape[0]
    w = ground_truth.shape[1]
    for i in range(0, h):
        for j in range(0, w):
            #print(ground_truth[i,j]) #,result_without_skin[i,j] )
            if ground_truth[i,j] <= 10:
                ground_truth[i,j] = 0
            else:
                ground_truth[i,j] = 1
                
            if prediction_final[i,j] <= 10:
                prediction_final[i,j] = 0
            else:
                prediction_final[i,j] = 1
                
                
    
    ground_truth = ground_truth.reshape(-1)
    prediction_final = prediction_final.reshape(-1)       
            
    print("Acc=", metrics.f1_score(ground_truth, prediction_final))

    
    cv2.imshow('Original image', img)
    cv2.imshow('Ground truth', img_mask)
    # cv2.imshow('Raw prediction from model', segmented)
    # cv2.imshow('Skin mask', skin_mask)
    # cv2.imshow('Skin mask filled', skin_mask_filled)
    # cv2.imshow('Prediction on skin', result_only_skin)
    cv2.imshow('Prediction final', result_without_skin)

    
    #cv2.imshow("after noise removal", out)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    
#####################################################
    
    #plt.imsave('Medtec-dataset/segmented/' + image, segmented, cmap='jet')



